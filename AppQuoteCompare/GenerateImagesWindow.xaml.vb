﻿Public Class GenerateImagesWindow

    Public Sub New(generateViewModel As GenerateImagesViewModel)

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        Me.DataContext = generateViewModel
    End Sub

    Private Sub Button_Click(sender As Object, e As RoutedEventArgs)
        Me.DialogResult = True
    End Sub
End Class
