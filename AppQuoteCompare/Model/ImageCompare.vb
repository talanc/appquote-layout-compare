﻿Imports System.IO

Public Class ImageCompare
    Inherits ObservableObject

    Private _name As String
    Public Property Name As String
        Get
            Return _name
        End Get
        Set(value As String)
            If value <> _name Then
                _name = value
                NotifyPropertyChanged()
            End If
        End Set
    End Property

    Private _expectedPath As String
    Public Property ExpectedPath As String
        Get
            Return _expectedPath
        End Get
        Set(value As String)
            If value <> _expectedPath Then
                _expectedPath = value
                NotifyPropertyChanged()

                RefreshMatchProperties()
            End If
        End Set
    End Property

    Private _actualPath As String
    Public Property ActualPath As String
        Get
            Return _actualPath
        End Get
        Set(value As String)
            If value <> _actualPath Then
                _actualPath = value
                NotifyPropertyChanged()

                RefreshMatchProperties()
            End If
        End Set
    End Property

    Private _isMatch As Boolean
    Public Property IsMatch As Boolean
        Get
            Return _isMatch
        End Get
        Private Set(value As Boolean)
            If _isMatch <> value Then
                _isMatch = value
                NotifyPropertyChanged()
            End If
        End Set
    End Property

    Private _isDifferent As Boolean
    Public Property IsDifferent As Boolean
        Get
            Return _isDifferent
        End Get
        Private Set(value As Boolean)
            If _isDifferent <> value Then
                _isDifferent = value
                NotifyPropertyChanged()
            End If
        End Set
    End Property

    Public Sub New()
    End Sub

    Public Sub New(filePath As String)
        Name = Path.GetFileNameWithoutExtension(filePath)
        ActualPath = filePath
        ExpectedPath = Path.Combine(Path.GetDirectoryName(filePath),
                                    "..", "Expected", Path.GetFileName(filePath))
    End Sub

    Private Sub RefreshMatchProperties()
        If Not File.Exists(ExpectedPath) Then
            IsMatch = False
        ElseIf ExpectedPath.StartsWith("http") OrElse ActualPath.StartsWith("http") Then
            IsMatch = (ExpectedPath = ActualPath)
        Else
            IsMatch = (File.ReadAllText(ExpectedPath) = File.ReadAllText(ActualPath))
        End If
        IsDifferent = Not IsMatch
    End Sub
End Class
