﻿Imports System.IO
Imports System.Collections.ObjectModel

Public Class TestDirectory
    Inherits ObservableObject

    Public Sub New()
    End Sub

    Public Sub New(directoryPath As String)
        Dim di As New DirectoryInfo(directoryPath)
        _name = di.Name
        _fullName = di.FullName
    End Sub

    Private _name As String
    Public Property Name() As String
        Get
            Return _name
        End Get
        Set(ByVal value As String)
            If _name <> value Then
                _name = value
                NotifyPropertyChanged()
            End If
        End Set
    End Property

    Private _fullName As String
    Public Property FullName() As String
        Get
            Return _fullName
        End Get
        Set(ByVal value As String)
            If _fullName <> value Then
                _fullName = value
                NotifyPropertyChanged()
            End If
        End Set
    End Property

    Private _compares As ObservableCollection(Of ImageCompare)
    Public ReadOnly Property Compares As IEnumerable(Of ImageCompare)
        Get
            If _compares Is Nothing Then
                Dim cmp = From filePath In IO.Directory.EnumerateFiles(FullName, "*.png")
                          Select New ImageCompare(filePath)

                _compares = New ObservableCollection(Of ImageCompare)(cmp)
            End If
            Return _compares
        End Get
    End Property
End Class