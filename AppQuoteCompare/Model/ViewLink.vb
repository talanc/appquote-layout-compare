﻿Imports System.IO

Public Class ViewLink
    Implements IEquatable(Of ViewLink), IComparable(Of ViewLink)

    Property Id As Integer = 0
    Property DistId As String = ""

    ReadOnly Property Url As String
        Get
            Dim link = Config.GetApplicationConfig().UrlViewAppQuote
            link &= "?" & "id=" & Id
            If DistId <> Nothing Then
                link &= "&distid=" & DistId
            End If
            Return link
        End Get
    End Property

    Public Overrides Function ToString() As String
        Return Id & DistId
    End Function

    Public Function ToFileName() As String
        Return Id & DistId & ".png"
    End Function

    Public Shared Function FromFilePath(filePath As String) As ViewLink
        Dim name = Path.GetFileNameWithoutExtension(filePath)

        Dim i As Integer = name.Length - 1
        While Char.IsLetter(name(i))
            i -= 1
        End While

        Dim vl As New ViewLink
        vl.Id = CInt(name.Substring(0, i + 1))
        vl.DistId = name.Substring(i + 1)
        Return vl
    End Function

    Public Overrides Function GetHashCode() As Integer
        Return (Id & DistId).GetHashCode()
    End Function

    Public Overrides Function Equals(obj As Object) As Boolean
        Return TypeOf obj Is ViewLink AndAlso Equals1(DirectCast(obj, ViewLink))
    End Function

    Public Function Equals1(other As ViewLink) As Boolean Implements IEquatable(Of ViewLink).Equals
        Return Me.Id = other.Id AndAlso Me.DistId = other.DistId
    End Function

    Public Function CompareTo(other As ViewLink) As Integer Implements IComparable(Of ViewLink).CompareTo
        Return Me.ToString().CompareTo(other.ToString())
    End Function
End Class
