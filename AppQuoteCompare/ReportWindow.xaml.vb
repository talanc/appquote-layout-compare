﻿Imports System.ComponentModel
Imports System.Collections.ObjectModel

Public Class ReportWindow

    Public Sub New(Optional reportViewModel As ReportViewModel = Nothing)

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        If reportViewModel Is Nothing Then
            reportViewModel = New ReportViewModel()
        End If

        Me.DataContext = reportViewModel
    End Sub

    Private Sub Image_MouseUp(sender As Object, e As MouseButtonEventArgs)
        Dim image = TryCast(sender, Image)
        If image IsNot Nothing Then
            Process.Start(image.Source.ToString())
        End If
    End Sub

    Private Sub Slider_ValueChanged(sender As Object, e As RoutedPropertyChangedEventArgs(Of Double))
        Dim sliderValue = CType(Me.FindResource("SliderValue"), Double)
        Dim gl As New GridLength(sliderValue - e.NewValue, GridUnitType.Star)

        Spacer1.Width = gl
        Spacer2.Width = gl
    End Sub

End Class
