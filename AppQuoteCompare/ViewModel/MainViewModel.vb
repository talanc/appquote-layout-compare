﻿Imports System.Collections.ObjectModel
Imports System.IO

Public Class MainViewModel
    Inherits ObservableObject

    Private Shared ReadOnly Property BasePath As String
        Get
            Return Config.GetApplicationConfig().DirTests
        End Get
    End Property

    Private Shared ReadOnly Property ExpectedDirectory As String
        Get
            Return Config.GetApplicationConfig().DirExpected
        End Get
    End Property

    Private Shared ReadOnly Property ExpectedPath As String
        Get
            Return IO.Path.Combine(BasePath, ExpectedDirectory)
        End Get
    End Property

    Public ReadOnly Property TestFolders As IEnumerable(Of TestDirectory)
        Get
            Dim dirs = From dir In Directory.EnumerateDirectories(BasePath)
                       Let td = New TestDirectory(dir)
                       Where td.Name <> ExpectedDirectory
                       Order By td.Name Descending
                       Select td
            Return New ObservableCollection(Of TestDirectory)(dirs)
        End Get
    End Property

    Private _navigateCommand As RelayCommand(Of TestDirectory)
    Public ReadOnly Property NavigateCommand As RelayCommand(Of TestDirectory)
        Get
            If _navigateCommand Is Nothing Then
                _navigateCommand = New RelayCommand(Of TestDirectory)(AddressOf ShowReport)
            End If
            Return _navigateCommand
        End Get
    End Property

    Private _showGenerateWindowCommand As RelayCommand
    Public ReadOnly Property ShowGenerateWindowCommand As RelayCommand
        Get
            If _showGenerateWindowCommand Is Nothing Then
                _showGenerateWindowCommand = New RelayCommand(Sub()
                                                                  Dim generateViewModel As New GenerateImagesViewModel()
                                                                  Dim generateView As New GenerateImagesWindow(generateViewModel)
                                                                  If generateView.ShowDialog() = True Then
                                                                      ShowReport(generateViewModel.TestDirectory)
                                                                  End If

                                                                  NotifyPropertyChanged("TestFolders") ' bad ???
                                                              End Sub)
            End If
            Return _showGenerateWindowCommand
        End Get
    End Property

    Private Sub ShowReport(testDirectory As TestDirectory)
        Dim reportViewModel As New ReportViewModel(testDirectory)
        Dim report As New ReportWindow(reportViewModel)
        report.ShowDialog()
    End Sub
End Class
