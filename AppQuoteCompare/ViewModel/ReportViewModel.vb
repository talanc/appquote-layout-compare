﻿Imports System.Collections.ObjectModel

Public Class ReportViewModel
    Inherits ObservableObject

    Public Sub New()
    End Sub

    Public Sub New(testDirectory As TestDirectory)
        Me.SelectedTestDirectory = testDirectory
        Me.Compares = testDirectory.Compares
    End Sub

    Private _selectedTestDirectory As TestDirectory
    Public Property SelectedTestDirectory() As TestDirectory
        Get
            Return _selectedTestDirectory
        End Get
        Set(ByVal value As TestDirectory)
            _selectedTestDirectory = value
            NotifyPropertyChanged()
        End Set
    End Property

    Private _compares As ObservableCollection(Of ImageCompare)
    Public Property Compares As IEnumerable(Of ImageCompare)
        Get
            Return _compares
        End Get
        Set(value As IEnumerable(Of ImageCompare))
            _compares = New ObservableCollection(Of ImageCompare)(value)
            NotifyPropertyChanged()
        End Set
    End Property

    Private _copyToExpectedCommand As RelayCommand(Of ImageCompare)
    Public ReadOnly Property CopyToExpectedCommand As RelayCommand(Of ImageCompare)
        Get
            If _copyToExpectedCommand Is Nothing Then
                _copyToExpectedCommand = New RelayCommand(Of ImageCompare)(
                    Sub(cmp)
                        IO.File.Copy(cmp.ActualPath, cmp.ExpectedPath, True)
                        cmp.ExpectedPath = cmp.ActualPath
                    End Sub
                )
            End If
            Return _copyToExpectedCommand
        End Get
    End Property

    Private Function ShowCommand_CanExecute() As Boolean
        Return SelectedTestDirectory IsNot Nothing AndAlso
            SelectedTestDirectory.Compares IsNot Nothing AndAlso
            SelectedTestDirectory.Compares.FirstOrDefault() IsNot Nothing
    End Function

    Private _showDiffCommand As RelayCommand
    Public ReadOnly Property ShowDiffCommand As RelayCommand
        Get
            If _showDiffCommand Is Nothing Then
                _showDiffCommand = New RelayCommand(Sub() Compares = SelectedTestDirectory.Compares.Where(Function(cmp) cmp.IsDifferent),
                                                    AddressOf ShowCommand_CanExecute)
            End If
            Return _showDiffCommand
        End Get
    End Property

    Private _showAllCommand As RelayCommand
    Public ReadOnly Property ShowAllCommand As RelayCommand
        Get
            If _showAllCommand Is Nothing Then
                _showAllCommand = New RelayCommand(Sub() Compares = SelectedTestDirectory.Compares,
                                                   AddressOf ShowCommand_CanExecute)
            End If
            Return _showAllCommand
        End Get
    End Property

End Class
