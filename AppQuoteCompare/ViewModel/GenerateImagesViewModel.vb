﻿Imports System.IO
Imports System.Collections.Concurrent
Imports System.Threading

Public Class GenerateImagesViewModel
    Inherits ObservableObject

    ' TODO fix
    ' only works with 1 worker
    ' find a way to use multiple workers
    Public Const WORKERS_NUM = 1

    Private _expectedPath As String = ""
    Private ReadOnly Property ExpectedPath As String
        Get
            If _expectedPath = "" Then
                Dim cfg = Config.GetApplicationConfig()
                _expectedPath = Path.GetFullPath(Path.Combine(cfg.DirTests, cfg.DirExpected))
            End If
            Return _expectedPath
        End Get
    End Property

    Private _generateCommand As RelayCommand
    Public ReadOnly Property GenerateCommand() As RelayCommand
        Get
            If _generateCommand Is Nothing Then
                _generateCommand = New RelayCommand(AddressOf GenerateTaskAsync)
            End If
            Return _generateCommand
        End Get
    End Property

    Private Async Sub GenerateTaskAsync()
        Await Task.Run(AddressOf GenerateTask)
    End Sub

    Private Sub GenerateTask()
        Running = True
        StartTime = DateTime.Now

        Progress = "Creating test directory"
        TestDirectory = GenerateTestDirectory()

        Progress = "Creating driver"
        Dim driver As New AppQuoteDriver()

        Progress = "Finding AppQuote elements"
        Dim links = driver.GetLinks(Config.GetApplicationConfig().UrlReport, Limit)

        NumLinks = links.Count

        If TestDirectory.FullName <> ExpectedPath Then
            Progress = "Adding elements from Expected directory"
            For Each filePath In IO.Directory.EnumerateFiles(ExpectedPath, "*.png")
                If Limit > 0 AndAlso NumLinks >= Limit Then
                    Exit For
                End If
                Dim link = ViewLink.FromFilePath(filePath)
                links.Add(link)
                NumLinks += 1
            Next
        End If

        CurrentLinkIndex = 0

        Dim bag As New ConcurrentBag(Of ViewLink)(links)

        ' Create worker tasks
        ' First worker uses the global driver
        ' All other workers uses their own local driver
        Dim tasks(0 To WORKERS_NUM - 1) As Task
        tasks(0) = Task.Run(Sub() WorkerTask(bag, driver))
        For i = 1 To tasks.Length - 1
            tasks(i) = Task.Run(Sub() WorkerTask(bag, Nothing))
        Next

        Task.WaitAll(tasks)

        driver.Close()

        Progress = "Complete"

        EndTime = DateTime.Now()

        TimeTakenInSeconds = CInt((EndTime - StartTime).TotalSeconds)

        Completed = True
        Running = False
    End Sub

    Private ReadOnly createDriverLock As New Object
    Private ReadOnly progressLock As New Object
    Private Sub WorkerTask(bag As ConcurrentBag(Of ViewLink), driver As AppQuoteDriver)
        Dim isLocalDriver = False
        If driver Is Nothing Then
            SyncLock createDriverLock
                If bag.Count < 10 Then
                    Return
                End If
                driver = New AppQuoteDriver()
            End SyncLock
            isLocalDriver = True
        End If

        Dim link As ViewLink = Nothing

        While bag.TryTake(link)
            SyncLock progressLock
                CurrentLinkIndex += 1
                Progress = "Processing " & link.ToString()
            End SyncLock

            Dim canvasName = link.ToFileName()
            Dim canvasPath = IO.Path.Combine(TestDirectory.FullName, canvasName)

            driver.SaveCanvas(link.Url, canvasPath)

            ' Copy to the expected directory if we can't find it there
            Dim expectedCanvasPath = Path.Combine(ExpectedPath, canvasName)
            If Not File.Exists(expectedCanvasPath) Then
                File.Copy(canvasPath, expectedCanvasPath, True)
            End If
        End While

        ' Close local driver
        If isLocalDriver Then
            driver.Close()
        End If
    End Sub

    Private _limit As Integer
    Public Property Limit() As Integer
        Get
            Return _limit
        End Get
        Set(ByVal value As Integer)
            If _limit <> value Then
                _limit = value
                NotifyPropertyChanged()
            End If
        End Set
    End Property

    Private _progress As String = "Progress"
    Public Property Progress() As String
        Get
            Return _progress
        End Get
        Set(ByVal value As String)
            If _progress <> value Then
                _progress = value
                NotifyPropertyChanged()
            End If
        End Set
    End Property

    Private _startTime As DateTime
    Public Property StartTime() As DateTime
        Get
            Return _startTime
        End Get
        Set(ByVal value As DateTime)
            If _startTime <> value Then
                _startTime = value
                NotifyPropertyChanged()
            End If
        End Set
    End Property

    Private _endTime As DateTime
    Public Property EndTime() As DateTime
        Get
            Return _endTime
        End Get
        Set(ByVal value As DateTime)
            If _endTime <> value Then
                _endTime = value
                NotifyPropertyChanged()
            End If
        End Set
    End Property

    Private _timeTakenInSeconds As Integer
    Public Property TimeTakenInSeconds() As Integer
        Get
            Return _timeTakenInSeconds
        End Get
        Set(ByVal value As Integer)
            If _timeTakenInSeconds <> value Then
                _timeTakenInSeconds = value
                NotifyPropertyChanged()
            End If
        End Set
    End Property

    Private _testDirectory As TestDirectory
    Public Property TestDirectory() As TestDirectory
        Get
            Return _testDirectory
        End Get
        Set(ByVal value As TestDirectory)
            If _testDirectory IsNot value Then
                _testDirectory = value
                NotifyPropertyChanged()
            End If
        End Set
    End Property

    Private _numLinks As Integer
    Public Property NumLinks() As Integer
        Get
            Return _numLinks
        End Get
        Set(ByVal value As Integer)
            If _numLinks <> value Then
                _numLinks = value
                NotifyPropertyChanged()
            End If
        End Set
    End Property

    Private _currentLinkIndex As Integer
    Public Property CurrentLinkIndex() As Integer
        Get
            Return _currentLinkIndex
        End Get
        Set(ByVal value As Integer)
            If _currentLinkIndex <> value Then
                _currentLinkIndex = value
                NotifyPropertyChanged()
            End If
        End Set
    End Property

    Private _running As Boolean
    Public Property Running() As Boolean
        Get
            Return _running
        End Get
        Set(ByVal value As Boolean)
            If _running <> value Then
                _running = value
                NotifyPropertyChanged()
            End If
        End Set
    End Property

    Private _completed As Boolean
    Public Property Completed() As Boolean
        Get
            Return _completed
        End Get
        Set(ByVal value As Boolean)
            If _completed <> value Then
                _completed = value
                NotifyPropertyChanged()
            End If
        End Set
    End Property

    Private Function GenerateTestDirectory() As TestDirectory
        Dim baseDir = Config.GetApplicationConfig().DirTests

        ' Use Expected directory if it's hasn't been created
        Dim expectedDir As New IO.DirectoryInfo(ExpectedPath)
        If Not expectedDir.Exists Then
            expectedDir.Create()
            Return New TestDirectory(expectedDir.FullName)
        End If

        Dim name As String = "Test-" & DateTime.Now.ToString("yyyy-MM-dd")
        Dim testDir = IO.Path.Combine(baseDir, name)
        Dim i = 2
        While IO.Directory.Exists(testDir)
            testDir = IO.Path.Combine(baseDir, name & "-v" & i)
            i += 1
        End While

        IO.Directory.CreateDirectory(testDir)
        Return New TestDirectory(testDir)
    End Function
End Class
