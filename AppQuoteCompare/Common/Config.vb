﻿Imports System.IO

Public Class Config
    Private Const CONFIG_PATH As String = "Config.xml"

    Public ReadOnly DirTests As String
    Public ReadOnly DirExpected As String

    Public ReadOnly UrlReport As String
    Public ReadOnly UrlViewAppQuote As String

    Private _envPaths As Dictionary(Of String, String)

    Public Sub New()
        ' Config file exists
        If Not File.Exists(CONFIG_PATH) Then
            MsgBox(CONFIG_PATH & " does not exist")
            Environment.Exit(1)
        End If

        ' Get config as XElement
        Dim root As XElement = Nothing
        Try
            root = XElement.Load(CONFIG_PATH)
        Catch ex As Exception
            MsgBox(ex.Message)
            Environment.Exit(1)
        End Try

        _envPaths = New Dictionary(Of String, String)
        For Each folderPath In GetEnvironmentFolders()
            Dim mask = "$" & folderPath.ToString()
            _envPaths.Add(mask, Environment.GetFolderPath(folderPath))
        Next

        DirTests = GetValue(root, "DirTests")
        DirExpected = GetValue(root, "DirExpected")

        UrlReport = GetValue(root, "UrlReport")
        UrlViewAppQuote = GetValue(root, "UrlViewAppQuote")
    End Sub

    Private Shared Function GetEnvironmentFolders() As IEnumerable(Of Environment.SpecialFolder)
        Return From v In [Enum].GetValues(GetType(Environment.SpecialFolder))
               Select CType(v, Environment.SpecialFolder)
               Distinct
    End Function

    Public Shared Function GetApplicationConfig() As Config
        Return CType(Application.Current, AppQuoteCompare.Application).Config
    End Function

    Private Function GetValue(root As XElement, name As String) As String
        Dim element = root.Element(name)
        If element Is Nothing Then
            MsgBox("Config error: " & name & " does not exist")
            Environment.Exit(1)
        End If

        ' TODO theres a better way to do this...
        Dim value As String = element.Value
        For Each kvp In _envPaths
            value = value.Replace(kvp.Key, kvp.Value)
        Next

        Return value
    End Function
End Class
