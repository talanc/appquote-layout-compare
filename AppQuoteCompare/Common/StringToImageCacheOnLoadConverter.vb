﻿Imports System.IO

' Putting this converter on an image source allows you to do stuff to the underlying image file.
' This is used in ReportWindow on our images. The user can copy the Actual image to the Expected image.
Public Class StringToImageCacheOnLoadConverter
    Implements IValueConverter

    Public Function Convert(value As Object, targetType As Type, parameter As Object, culture As Globalization.CultureInfo) As Object Implements IValueConverter.Convert
        Dim filePath = CStr(value)
        If File.Exists(filePath) Then
            Dim bitmap As New BitmapImage()
            bitmap.BeginInit()
            bitmap.CacheOption = BitmapCacheOption.OnLoad
            bitmap.UriSource = New Uri(filePath)
            bitmap.EndInit()
            Return bitmap
        End If
        Return Nothing
    End Function

    Public Function ConvertBack(value As Object, targetType As Type, parameter As Object, culture As Globalization.CultureInfo) As Object Implements IValueConverter.ConvertBack
        Dim bitmap = TryCast(value, BitmapImage)
        If bitmap IsNot Nothing Then
            Return bitmap.BaseUri.AbsolutePath
        End If
        Return Nothing
    End Function
End Class
