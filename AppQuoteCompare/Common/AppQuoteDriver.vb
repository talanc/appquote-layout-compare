﻿Imports OpenQA.Selenium
Imports OpenQA.Selenium.Firefox
Imports OpenQA.Selenium.Support.UI

Public Class AppQuoteDriver
    Implements IDisposable

    Private _driver As IWebDriver

    Public Sub New()
        _driver = New FirefoxDriver()
    End Sub

    Public Sub Close()
        _driver.Close()
    End Sub

    Public Function GetLinks(url As String, Optional limit As Integer = -1) As SortedSet(Of ViewLink)
        ' TODO
        ' instead of driver use webclient / httpclient
        ' download as string and parse elements

        _driver.Navigate().GoToUrl(url)
        Dim elements = From element In _driver.FindElements(By.CssSelector("[data-appquote]"))
                       Select element
        If limit > 0 Then
            elements = elements.Take(limit)
        End If

        Dim links As New SortedSet(Of ViewLink)
        For Each element In elements
            Dim link As New ViewLink()
            link.Id = Integer.Parse(element.GetAttribute("data-id"))
            link.DistId = element.GetAttribute("data-distid")
            links.Add(link)
        Next

        Return links
    End Function

    Public Sub SaveCanvas(url As String, canvasPath As String)
        _driver.Navigate().GoToUrl(url)

        ' Wait for our canvas to load
        Dim element As IWebElement
        Try
            Dim wait As New WebDriverWait(_driver, TimeSpan.FromSeconds(5))
            element = wait.Until(ExpectedConditions.ElementExists(By.CssSelector("[data-loaded]")))
        Catch ex As WebDriverTimeoutException
            Return
        End Try

        ' Take a screenshot of the page
        Dim ts = CType(_driver, ITakesScreenshot)
        Dim screenshot = ts.GetScreenshot()

        ' Crop screenshot to show just the canvas
        Dim ms As New IO.MemoryStream(screenshot.AsByteArray)
        Using bmp As New System.Drawing.Bitmap(ms)
            Dim canvasRect As New System.Drawing.Rectangle(element.Location, element.Size)
            Using canvasBmp = bmp.Clone(canvasRect, bmp.PixelFormat)
                canvasBmp.Save(canvasPath)
            End Using
        End Using
    End Sub

#Region "IDisposable Support"
    Private disposedValue As Boolean ' To detect redundant calls

    ' IDisposable
    Protected Overridable Sub Dispose(disposing As Boolean)
        If Not Me.disposedValue Then
            If disposing Then
                ' TODO: dispose managed state (managed objects).
                _driver.Dispose()
            End If

            ' TODO: free unmanaged resources (unmanaged objects) and override Finalize() below.
            ' TODO: set large fields to null.
        End If
        Me.disposedValue = True
    End Sub

    ' TODO: override Finalize() only if Dispose(ByVal disposing As Boolean) above has code to free unmanaged resources.
    'Protected Overrides Sub Finalize()
    '    ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
    '    Dispose(False)
    '    MyBase.Finalize()
    'End Sub

    ' This code added by Visual Basic to correctly implement the disposable pattern.
    Public Sub Dispose() Implements IDisposable.Dispose
        ' Do not change this code.  Put cleanup code in Dispose(disposing As Boolean) above.
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub
#End Region

End Class
