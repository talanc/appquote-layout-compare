﻿Public Class RelayCommand
    Implements ICommand

    Private Shared Function AlwaysExecute() As Boolean
        Return True
    End Function

    Private _execute As Action
    Private _canExecute As Func(Of Boolean)

    Public Sub New(execute As Action, Optional canExecute As Func(Of Boolean) = Nothing)
        If execute Is Nothing Then
            Throw New ArgumentNullException("execute")
        End If
        If canExecute Is Nothing Then
            canExecute = AddressOf AlwaysExecute
        End If

        Me._execute = execute
        Me._canExecute = canExecute
    End Sub

    Public Function CanExecute(parameter As Object) As Boolean Implements ICommand.CanExecute
        Return _canExecute()
    End Function

    Public Custom Event CanExecuteChanged As EventHandler Implements ICommand.CanExecuteChanged
        AddHandler(value As EventHandler)
            AddHandler CommandManager.RequerySuggested, value
        End AddHandler

        RemoveHandler(value As EventHandler)
            RemoveHandler CommandManager.RequerySuggested, value
        End RemoveHandler

        RaiseEvent(sender As Object, e As EventArgs)
        End RaiseEvent
    End Event

    Public Sub Execute(parameter As Object) Implements ICommand.Execute
        _execute()
    End Sub
End Class

Public Class RelayCommand(Of T)
    Implements ICommand

    Private Shared Function AlwaysExecute(obj As T) As Boolean
        Return True
    End Function

    Private _execute As Action(Of T)
    Private _canExecute As Predicate(Of T)

    Public Sub New(execute As Action(Of T), Optional canExecute As Predicate(Of T) = Nothing)
        If execute Is Nothing Then
            Throw New ArgumentNullException("execute")
        End If
        If canExecute Is Nothing Then
            canExecute = AddressOf AlwaysExecute
        End If

        Me._execute = execute
        Me._canExecute = canExecute
    End Sub

    Public Function CanExecute(parameter As Object) As Boolean Implements ICommand.CanExecute
        Return _canExecute(CType(parameter, T))
    End Function

    Public Custom Event CanExecuteChanged As EventHandler Implements ICommand.CanExecuteChanged
        AddHandler(value As EventHandler)
            AddHandler CommandManager.RequerySuggested, value
        End AddHandler

        RemoveHandler(value As EventHandler)
            RemoveHandler CommandManager.RequerySuggested, value
        End RemoveHandler

        RaiseEvent(sender As Object, e As EventArgs)
        End RaiseEvent
    End Event

    Public Sub Execute(parameter As Object) Implements ICommand.Execute
        _execute(CType(parameter, T))
    End Sub
End Class
